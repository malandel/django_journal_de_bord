# Journal de bord - équipe pédagogique - Simplon

Construire un outil qu'utilisera votre équipe pédagogique pour journaliser les événements collectifs et individuels qui se déroulent dans la formation.

Utiliser un :snake: framework Python :snake:

## Pré-requis

- Python3
- PostgreSQL
- un système de gestion d'environnement virtuel pour Python (Miniconda, vitualenv, pipenv)

## Créer la base de données

J'ai choisi de créer ma base de données avec une plateforme d'administration pour PostgreSQL (PgAdmin4).
Si vous souhaitez faire autrement, référez-vous à la documentation officielle de PostgreSQL.

## Installation du projet en local

- Créer un environnement virtuel avec Python 3

```
git clone
cd simplon
pip3 install requirements.txt
```

- Copiez le fichier db_config_copy.py et renommez-le db_config.py puis renseignez vos informations de base de données.

- Appliquer les migrations : ```python3 manage.py migrate ```

- Créer le super utilisateur pour accéder au site : ```python3 manage.py createsuperuser```. Notez les informations (user et password) que vous renseignez pour pouvoir vous connecter au site.

-  Charger les données initiales : ```python3 manage.py loaddata categories.json``` et ```python3 manage.py loaddata apprenants.json```

- Lancer le serveur : ```python3 manage.py runserver``` et accéder au programme : http://127.0.0.1:8000/home/