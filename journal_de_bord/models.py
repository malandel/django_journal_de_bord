from django.db.models import Model
from django.db.models.deletion import CASCADE
from django.db.models.fields import CharField, DateField, TextField
from django.db.models.fields.related import ForeignKey, ManyToManyField
from django.contrib.auth.models import User

# Create your models here.

class Apprenants(Model):
    nom = CharField(max_length=256, blank=False)
    prenom = CharField(max_length=256, blank=False)
    email = CharField(max_length=256, blank=True)

    class Meta:
        verbose_name = "apprenant.e"
        verbose_name_plural = "apprenant.e.s"
    
    def __str__(self):
        return f"{self.prenom} {self.nom}"

class Categories(Model):
    titre = CharField(max_length=256, blank=False)

    class Meta:
        verbose_name = "catégorie"
        verbose_name_plural = "catégories"

    def __str__(self):
        return f"{self.titre}"

    

class Entrees(Model):
    titre = CharField(max_length=256, blank=False)
    auteur = ForeignKey(User, on_delete=CASCADE, blank=False)
    contenu = TextField(blank=False)
    date_creation = DateField(auto_now_add=True)
    date_modification = DateField(auto_now=True)
    apprenants = ManyToManyField(Apprenants, blank=True)
    categories = ManyToManyField(Categories)

    class Meta:
        ordering = ['-date_creation']
        verbose_name = "entrée"
        verbose_name_plural = "entrées"

    def __str__(self):
        return f"{self.titre} - {self.auteur}"



