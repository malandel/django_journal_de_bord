from django.shortcuts import render
from .models import Entrees, Apprenants, Categories
from django.contrib.auth.decorators import login_required

@login_required
def home(request):
    data = Entrees.objects.all()
    apprenants = Apprenants.objects.all()
    categories = Categories.objects.all()
    if request.POST.get('apprenants'):
        apprenant = request.POST['apprenants']
        data = Entrees.objects.filter(apprenants = apprenant)
    if request.POST.get('categories'):
        categorie = request.POST['categories']
        data = Entrees.objects.filter(categories = categorie)

    return render (request, 'home.html', {'data' : data, 'apprenants':apprenants, 'categories' : categories})
