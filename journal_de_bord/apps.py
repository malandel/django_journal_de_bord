from django.apps import AppConfig


class JournalDeBordConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'journal_de_bord'
