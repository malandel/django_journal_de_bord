from django.contrib import admin
from .models import Entrees, Categories, Apprenants
from django.forms.widgets import CheckboxSelectMultiple
from django.db import models

# Register your models here.

admin.site.site_header = "Simplon - Équipe pédagogique"
admin.site.site_title  =  "Simplon"
admin.site.index_title  =  "Journal de bord des promos"

@admin.register(Entrees)
class EntreesAdmin(admin.ModelAdmin):
    list_filter = ("apprenants","categories", "auteur", "date_creation", "date_modification" )
    list_display = ["titre", "auteur", "contenu","date_creation", "date_modification"]
    sortable_by = ("date_creation")
    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple},
    }

@admin.register(Apprenants)
class ApprenantsAdmin(admin.ModelAdmin):
    list_display = ["prenom", "nom", "email"]

admin.site.register(Categories)
